A jupyter notebook containing basics to learn Python.

To read this notebook, download and install [Python](https://www.python.org/downloads/), then follow the instructions to install Jupyter Notebook [here](https://jupyter.org/install).
Finally, run `jupyter notebook "Python basics.ipynb"` to read the notebook.