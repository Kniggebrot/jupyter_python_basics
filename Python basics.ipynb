{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Python basics\n",
    "---\n",
    "This notebook will discuss some Python 3 basics, starting with general syntax, continuing with variables and flow control and finishing with interaction with the OS using the os-module."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic example: Hello World\n",
    "\n",
    "Let's look at the following example code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Hello World!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, the function `print()` outputs the string input (the letters/words encloded in quotation marks) to the standard output, in this case below the code block.\n",
    "If you already know other programming languages, you may have noticed the lack of a character ending the line, or no include. This is because `print()` is a built-in function; functions from external modules can be used with the `import` statement:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os    # To only import a specific function from a module, \"from module import function\" can be used,\n",
    "             # e.g. \"from os import getcwd\"\n",
    "\n",
    "print('Hello there!')\n",
    "print(f\"You are running this notebook from: {os.getcwd()}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this block of code, three new things can be noticed:\n",
    "\n",
    "- **Comments**: Everything written behind a '#' will be ignored by the Python interpreter. Thus, you can comment your code to e.g. explain it's function or note missing features in the line behind a '#'.\n",
    "  - However, for multiline comments this will have to be repeated for each comment line as Python has no multiline comment character (sequence); an informal way to realise multiline comments will be introduced with multiline [strings](#mult_str).\n",
    "- **Strings encased in single quote marks**: In Python, a string can be encased in single or double quotes; the interpreter will treat both the same way.\n",
    "- **f-Strings**: Sooner or later you will encounter a situation in which you want to output something non-constant in a print-string, such as the result of a function. The easiest way to realize this is using f-Strings: by writing an 'f' in front of the string, Python will look for expressions enclosed in curly braces `{}` and replace them with their result / value as a string. In this example, it is the directory in which you have downloaded this notebook.\n",
    "  - Valid expressions to be enclosed are functions or evaluatable expressions, e.g. `os.getlogin()` or `1+1==2`, and also constant, non-string values, such as `7`, `True` or variables, which will be discussed in the following section. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Variables\n",
    "\n",
    "You can use variables to store values which e.g. aren't known at the time of writing the code, such as inputs from the user. Values can be assigned to a variable by writing an eqation in the form of \"*variable* = *value*\" e.g. `number = 7`.\n",
    "\n",
    "Contrary to other programming languages, a variable does not have to be assigned to a type before its usage, the type is determined by its input. But it is easy to cast a variable from one type to another using a function with the type name, like `int()` or `str()`. These functions will be discussed in the sections of their corresponding type."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Numbers\n",
    "##### Integers\n",
    "\n",
    "Take a look at the following example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Just add 2\n",
    "\n",
    "number_str = input(\"Please input an integer: \")\n",
    "number     = int(number_str)\n",
    "print(f\"The result of adding 2 to this number is: {number+2}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Two new things appear in this block of code:\n",
    "\n",
    "- The `input()` function first prints its input and then allows the user to enter any input, which will be stored in the variable on the left hand side.\n",
    "- However, `input()` stores its input as a string, to which you cannot simply add 2; thus, `int()` is used to convert the string to an integer.\n",
    "\n",
    "Please try the following:\n",
    "\n",
    "- What happens if you input letters or words instead of an integer?\n",
    "- And what happens if you input a decimal number?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Decimal numbers\n",
    "\n",
    "We can make our example work for decimal numbers by altering it in the following way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Just add 2\n",
    "\n",
    "number_str = input(\"Please input a number: \")\n",
    "number     = float(number_str)\n",
    "print(f\"The result of adding 2 to this number is: {number+2}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By replacing `int()` with `float()`, we cast our input into a decimal number, to which we can add 2. Only decimal numbers written with dots will be accepted, not with commas! Classic integers will also be accepted. \n",
    "\n",
    "In fact, Python will change the type of your number variable if necessary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Change int to float; other way around is only possible if float is already an integer!\n",
    "\n",
    "a = 2\n",
    "print(f\"a ({a})is a {type(a)}.\")\n",
    "a += 1.1         # += is shorthand for 'variable' + 'right hand side', here \"a + 1.1\"\n",
    "print(f\"a ({a})is a {type(a)} now!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please note that this is exclusive to number types only, and will not happen if you force a (possibly invalid) cast with `int()` or `float()`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Calculation operators\n",
    "\n",
    "To add, substract, multiply or divide numbers, use `+`, `-`, `*` or `/`:\n",
    "\n",
    "|Operator | Example|\n",
    "|---------|-------|\n",
    "| +   | 1 + 1 = 2|\n",
    "| -   | 1 - 1 = 0|\n",
    "| \\*  | 2 \\* 2 = 4|\n",
    "| /   | 3 / 2 = 1.5|\n",
    "\n",
    "There are also operators for more advanced calculations. With `a**b` you can raise a number *a* to the power of *b*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = float(input(\"Please enter a base: \"))\n",
    "b = float(input(\"Please enter an exponent: \"))\n",
    "c = a**b\n",
    "\n",
    "print(f\"{a} raised to the power of {b} is: {c}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using `//`, the result of a division will be floored:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = float(input(\"Please input a number: \"))\n",
    "b = float(input(\"Please input another number: \"))\n",
    "c = a // b\n",
    "\n",
    "print(f\"{a} divided by {b} is: {c} (plus modulus)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And with `%`, the modulus of a floored division can be calculated:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = float(input(\"Please input a number: \"))\n",
    "b = float(input(\"Please input another number: \"))\n",
    "c = a % b\n",
    "\n",
    "print(f\"The modulus of {a} divided by {b} is: {c}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "| Operator | Example |\n",
    "|----------|---------|\n",
    "| \\*\\* | 2 \\*\\* 3 = 8 |\n",
    "| //   | 3 // 2 = 1 |\n",
    "| % | 5 % 3 = 2 |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Basic assignment operators\n",
    "\n",
    "At the beginning of this section, you have already met the first assignment operator, '='. As seen in the various examples before this section, it assignes the result or the output of the right hand side to the variable(s) on the left hand side."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But in some cases, you might want to alter the (unknown or changing) value of a variable by adding, subtracting, ... another value to itself, e.g. in loops, where you want to use a variable as a counter for the amount of times the loop has been run. To save you some typing, a few shorthand operators can be used:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 3        # Important: You cannot initialize a variable with one of the following shorthands, it has to have a value first.\n",
    "b = 2\n",
    "\n",
    "a += 1       # is equal to a = a + 1\n",
    "a -= b       # is equal to a = a - b\n",
    "a *= 1       # is equal to a = a * 1\n",
    "a /= b       # is equal to a = a / b\n",
    "\n",
    "a **= b      # is equal to a = a ** b\n",
    "a //= 9      # is equal to a = a // 9\n",
    "a %= b       # is equal to a = a % b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "| Operator | Expanded expression |\n",
    "|----------|---------|\n",
    "| a += b | a = a + b |\n",
    "| a -= b | a = a - b |\n",
    "| a \\*= b | a = a \\* b |\n",
    "| a /= b | a = a / b |\n",
    "| a \\*\\*= b | a = a \\*\\* b |\n",
    "| a //= b | a = a // b |\n",
    "| a %= b | a = a % b |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are also a lot of assignment shorthands for bitwise operations, but these will not be covered here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Strings\n",
    "\n",
    "Single letters, words and sentences can be saved in strings. Everything enclosed in single quotes (`'`) or double quotes (`\"`) is a string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "string   = \"This is \"\n",
    "string_2 = \"a string!\"\n",
    "total    = string + string_2\n",
    "\n",
    "print(total)\n",
    "\n",
    "more    = \"\"\"\n",
    "Using 3 single or double quotes,\n",
    "a string spaning multiple lines\n",
    "can be written.\n",
    "\"\"\"\n",
    "print(more)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Strings can also be concatenated by simply adding them. Similar to numbers, only strings can be added to strings: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [],
   "source": [
    "string  = \"Number of the day: \"+\"5\"  # works\n",
    "print(string)\n",
    "string2 = \"Number of the day: \"+str(7) # also works\n",
    "print(string2)\n",
    "stwrng  = \"Number of the day: \"+9    # will throw an exception\n",
    "print(stwrng)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='mult_str'>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Values can be converted to strings using `str()`.\n",
    "\n",
    "Using multiline strings, multiline comments can be written by not assinging a multiline string to a variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "'''\n",
    "In this multistring, I can write\n",
    "as many lines as I want, but the\n",
    "interpreter will ignore everything\n",
    "between them because they aren't assigned\n",
    "to anything. This string will result in no\n",
    "output!\n",
    "'''\n",
    "print(\"As long as functions following this string exist...\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Booleans\n",
    "\n",
    "Booleans are variables with two possible states: *True* and *False*. They are often used to control the path a program takes, which will be discussed in [flow control](#Flow-control). Their values can be assigned directly, or based on comparing expressions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Bool1 = True                 # Feel free to experiment with the assigned values!\n",
    "print(f\"Bool1 is {Bool1}.\")\n",
    "\n",
    "Bool2 = (2 + 2 == 4)         # Notice the usage of two equal signs for comparing values instead of one for assigning values!\n",
    "\n",
    "print(\"Is Bool2 true?\")\n",
    "if Bool2 == True:            # The \"== True\" is optional as Bool2 is already a bool; more in flow control\n",
    "    print(\"It is!\")\n",
    "else:\n",
    "    print(\"No, it is False.\")\n",
    "    \n",
    "Bool3 = bool(9000)\n",
    "print(f\"Bool3 is {Bool3}.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With `bool()`, any type of variable is converted to a boolean:\n",
    "\n",
    "- If the variable contains anything and is **not** zero, it is *True*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Test1 = 90\n",
    "Test2 = \"Ein Test\"\n",
    "\n",
    "Bool1 = bool(Test1)\n",
    "Bool2 = bool(Test2)\n",
    "\n",
    "print(f\"{Bool1} and {Bool2}.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- If the variable is zero or *None*, a keyword meaning this variable contains truly nothing and is \"empty\", it is *False*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Test3 = 0\n",
    "Test4 = \"\"\n",
    "Test5 = []     # A list! Albeit being empty, it is an advanced variable type.\n",
    "\n",
    "print(f\"The variables are {bool(Test3)}, {bool(Test4)} and {bool(Test5)}.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Comparison operators\n",
    "\n",
    "Comparing expressions using comparing operators will always return a *True* or *False*: The left side will be compared to the right side, and if it fits to the operator, it will be *True*.\n",
    "\n",
    "Example: `1 > 2`\n",
    "\n",
    "Is 1 greater than 2? *No, so it returns False.*\n",
    "\n",
    "`3 > 2`\n",
    "Is 3 greater than 2? *Yes, so it returns True.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = (1 == 2)  # Is 1 equal to 2?\n",
    "print(f\"1 == 2? It's {a}.\")\n",
    "\n",
    "a = (1 != 2)  # Is 1 not equal to 2?\n",
    "print(f\"1 != 2? It's {a}.\")\n",
    "\n",
    "a = (2 > 2)  # Is 2 greater than 2?\n",
    "print(f\"2 > 2? It's {a}.\")\n",
    "\n",
    "a = (1 < 2)  # Is 2 less than 2?\n",
    "print(f\"1 < 2? It's {a}.\")\n",
    "\n",
    "a = (1 <= 2)  # Is 1 less or equal to 2?\n",
    "print(f\"1 <= 2? It's {a}.\")\n",
    "\n",
    "a = (1 >= 2)  # Is 1 greater or equal to 2?\n",
    "print(f\"1 >= 2? It's {a}.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below is a list of all comparing operators:\n",
    "\n",
    "| Operator | Example | Result |\n",
    "|----------|---------|--------|\n",
    "| == | 1 == 2 | False |\n",
    "| != | 1 != 2 | True |\n",
    "| < | 1 < 2 | True |\n",
    "| > | 1 > 2 | False |\n",
    "| <= | 1 <= 2 | True |\n",
    "| >= | 2 >= 2 | True |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Usually, comparing expressions are used for [flow control](#Flow-control)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Advanced variable types\n",
    "\n",
    "Often, you might want to store a sequence of e.g. inputs in just one variable, so your code can be simpler and less confusing. To achieve this, an advanced variable type like a list can be used, to which various values, even of different types, can be added to or read from. Even for more special cases such as a key-value list or unchangeable arrays Python is providing advanced data types natively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Lists\n",
    "\n",
    "Lists are variables which can store multiple values in one variable. Their special feature is that the order of the elements in the list is fixed, but their values aren't; they can be changed during a script."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list = [\"A string\",12,4.5]  # To initialize a variable as a list, use square brackets []. It can hold variables of multiple types.\n",
    "\n",
    "print(list[0])              # To access a certain member of a list, you can use its position in it\n",
    "                            # (starting from the left with zero)\n",
    "print(list[-1])             # To count from the right, use negative indexes\n",
    "\n",
    "print(list[1:])             # To access a range of entries, separate the indexes of the interval with a colon : \n",
    "                            # But the last value on the right will not be accessed!\n",
    "print(list[:1])             # If a side is left empty, it will start from the beginning / end at the end. Negative indexes can\n",
    "                            # also be used here.\n",
    "\n",
    "list[1] = \"This was an integer.\" # Now we have changed 12 to this string!\n",
    "print(list[1])\n",
    "\n",
    "# append/insert\n",
    "# remove/pop\n",
    "# copy\n",
    "# join\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- eval() to read inputs correctly!\n",
    "\n",
    "#### Tuples\n",
    "\n",
    "#### Sets\n",
    "\n",
    "#### Dictionaries"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Flow control\n",
    "\n",
    "### If, Elif, and Else\n",
    "\n",
    "(in for items in advanced datatypes)\n",
    "\n",
    "### Loops\n",
    "\n",
    "(looping advanced datatypes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def xplus2(x):\n",
    "    return x + 2\n",
    "\n",
    "y = float(input(\"Enter a number: \"))\n",
    "print(f\"{y} + 2 = {xplus2(y)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Modules"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
